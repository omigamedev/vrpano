// VR Pano for Mentalray
// by Omar Mohamed Ali Mudhir 
// omarator@gmail.com / @omigamedev

#pragma once

#include <math.h>
#include <shader.h>

typedef struct 
{
	miInteger m_cam_mode;
	miInteger m_render_mode;
	miScalar m_dist;
} vr_pano_params;

DLLEXPORT int vr_pano_version();
DLLEXPORT miBoolean vr_pano(miColor* out_pResult, miState* state, vr_pano_params* in_pParams);
DLLEXPORT void vr_pano_init(miState* state, vr_pano_params* in_pParams, miBoolean* inst_req);
DLLEXPORT void vr_pano_exit(miState* state, vr_pano_params* in_pParams);
