// VR Pano for Mentalray
// by Omar Mohamed Ali Mudhir 
// omarator@gmail.com / @omigamedev

#include <math.h>
#include <shader.h>
#include "vr_pano.h"

//static void* links[] = { fprintf, vsnprintf };

DLLEXPORT int vr_pano_version()
{
	return 1;
}

DLLEXPORT void vr_pano_init(miState* state, vr_pano_params* in_pParams, miBoolean* inst_req)
{
	if(!in_pParams) 
	{		
		*inst_req = miFALSE;
	} 
}

DLLEXPORT miBoolean vr_pano(miColor* out_pResult, miState* state, vr_pano_params* in_pParams)
{
	miScalar uval, vval;
	miVector raydir, raydir_internal; 
	miVector rayorig, rayorig_internal;
	miScalar sgn = 1.0f;
	miScalar eye_dist = 0.2f;
	miInteger cam_mode = 0;
	miInteger render_mode = 0;
	
	eye_dist = *mi_eval_scalar(&in_pParams->m_dist);
	cam_mode = *mi_eval_integer(&in_pParams->m_cam_mode);
	render_mode = *mi_eval_integer(&in_pParams->m_render_mode);

	if (cam_mode == 0) // Full sphere
    {
        uval = state->raster_x / state->camera->x_resolution;
        if (render_mode == 0) // Single mode left
        {
            vval = (state->camera->y_resolution - state->raster_y) / state->camera->y_resolution;
        }
        else if (render_mode == 1) // Single mode right
        {
            vval = (state->camera->y_resolution - state->raster_y) / state->camera->y_resolution;
            sgn = -1.f;
        }
        else if (render_mode == 2) // BottomUp mode
        {
            vval = 2.0f * (state->camera->y_resolution - state->raster_y) / state->camera->y_resolution;
	        if (vval > 1.f)
	        {
		        vval -= 1.f;
		        sgn = -1.f;
	        }
        }
    }
	else if (cam_mode == 1) // Split mode
    {
        uval = .25f * state->raster_x / state->camera->x_resolution + .5f - .125f;
        vval = .5f * (state->camera->y_resolution - state->raster_y) / state->camera->y_resolution + .25f;
    }


	miScalar anglex = (miScalar)M_PI * (2.0f * uval + 0.5f);
    miScalar angley = (miScalar)M_PI * vval;
    if (cam_mode == 2) // Card mode
    {
        mi_vector_to_camera(state, &raydir, &state->dir);
        mi_point_to_camera(state, &rayorig, &state->org);
        angley = acosf(raydir.z);
        anglex = atanf(raydir.y / raydir.x);
    }

	miScalar sx = sinf(anglex);
	miScalar cx = cosf(anglex);

	raydir.x = sinf(angley) * cx;
	raydir.y = cosf(angley);
	raydir.z = sinf(angley) * sx;
	mi_vector_from_camera(state, &raydir_internal, &raydir);
	
	rayorig.x = sgn * sx * eye_dist;
	rayorig.y = 0;
	rayorig.z = -sgn * cx * eye_dist;
	mi_point_from_camera(state, &rayorig_internal, &rayorig);

	return mi_trace_eye(out_pResult, state, &rayorig_internal, &raydir_internal);
}

DLLEXPORT void vr_pano_exit(miState* state, vr_pano_params* in_pParams)
{
}
