// VR Pano for Mentalray
// by Omar Mohamed Ali Mudhir 
// omarator@gmail.com / @omigamedev

CC = g++

MAYA_DIR = /Applications/Autodesk/mentalrayForMaya2014
OUT_INCLUDE = $(MAYA_DIR)/shaders/include/
OUT_LIB = $(MAYA_DIR)/shaders/vr_pano.dylib


INC = -I$(MAYA_DIR)/devkit/include 
LIB = 
LIB_STATIC = 
CFLAGS = -O3 -fPIC -DBIT64 -dynamic -fno-common -Wno-deprecated
LIBTOOL = libtool

OBJS = vr_pano.o

SRCS = vr_pano.cpp

all: vr_pano.dylib 
	cp vr_pano.mi $(OUT_INCLUDE)

$(OBJS): vr_pano.cpp
	$(CC) $(CFLAGS) $(INC) $(LIB) -c $*.cpp $(LIB_STATIC)

vr_pano.dylib : $(OBJS) 
	$(LIBTOOL) -flat_namespace -undefined suppress -dynamic -o $(OUT_LIB)  $(OBJS) 

clean: 
	rm -f $(OBJS) 
	rm -f vr_pano.dylib 
